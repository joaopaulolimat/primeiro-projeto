terraform {
  backend "s3" {
    bucket = "base-config-343951"
    key    = "gitlab-runner-fleet"
    region = "us-east-1"
  }
}
